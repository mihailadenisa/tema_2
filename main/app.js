function addTokens(input, tokens){
	
	if(!(typeof input === 'string')){
		throw new Error('Invalid input');
	}
	
	if(input.length < 6){
		throw new Error('Input should have at least 6 characters');
	}
	
	let ok = true;
	for(i of tokens){
		let val = Object.values(i);
		if(val.length > 1 || typeof val[0] !== 'string'){
			ok = false;
		}
	}
	if(ok === false){
		throw new Error('Invalid array format');
	}
	
	if((input.indexOf('...') !== -1) && input.match(/\.\.\./g).length === tokens.length){
		for(i of tokens){
			input = input.replace('...', "${"+Object.values(i)[0]+"}");
		}
	}
	return input;
}

const app = {
    addTokens: addTokens
}

module.exports = app;